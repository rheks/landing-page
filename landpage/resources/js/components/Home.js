import React from 'react';
import ReactDOM from 'react-dom';

function Home() {
    return (
        <section class="home">
        <div class="container">
            <div class="row fullscreen">
                <div class="homeContent">
                    <div class="block">
                        <p class="head1">Fast Transaction </p>
                        <p class="head1">For All People</p>
                        <p class="head2">#SaveYourTime</p>
                        <div class="btnDownload">
                            <a href="#">Download Now</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    );
}

export default Home;

if (document.getElementById('Home')) {
    ReactDOM.render(<Home />, document.getElementById('Home'));
}
