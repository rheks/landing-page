import React from 'react';
import ReactDOM from 'react-dom';

function Navbar() {
    return (
        <header>
            <div class="container">
                <div class="row">
                    <div class="brandName">
                        <a href="#" class="name">Dompet.ID</a>
                    </div>
                    <div class="hamburger">
                        <i class="fas fa-bars"></i>
                    </div>
                    <nav class="navbar">
                        <ul>
                            <li><a href="#">Home</a></li>
                            <li><a href="#">About</a></li>
                            <li><a href="#">Products</a></li>
                            <li><a href="#">Merchant</a></li>
                            <li><a href="#">Blog</a></li>
                            <li><a href="#">Help</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </header>
    );
}

export default Navbar;

if (document.getElementById('Navbar')) {
    ReactDOM.render(<Navbar />, document.getElementById('Navbar'));
}
