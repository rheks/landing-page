<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="description" content="Dompet.ID Official Website">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dompet.ID | Official Website</title>
    <!-- Favicon -->
    <link rel="shortcut icon" href="img/Logo.png" type="image/x-icon">
    <!-- Favicon -->

    <!-- CSS Style -->
    <link rel="stylesheet" href="css/style.css">
    <!-- CSS Style -->
    
    <!-- Fontawesome CSS -->
    <link rel="stylesheet" href="css/all.css">
    <!-- Fontawesome CSS --> 
</head>
<body>
    @yield('Navbar')

    @yield('Home')
    
    <script src="{{ asset('js/app.js') }}"></script> 
    <script src="js/script.js"></script>
</body>
</html>